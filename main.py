import pygame,sys
import random
import math
import config
from pygame.font import FontType
from pygame.ftfont import Font
from pygame.locals import *
from config import *
pygame.init()
screen = pygame.display.set_mode((800, 640))
# title and logo
pygame.display.set_caption("Run to Live")
icon = pygame.image.load('spider.png')
pygame.display.set_icon(icon)

s_font = pygame.font.Font('freesansbold.ttf', 40)


def player(playerx, playery):
    screen.blit(plrimg, (playerx, playery))


plrimg = pygame.image.load('superhero.png')


def mo(mox, moy):
    screen.blit(moimage, (mox, moy))


moimage = pygame.image.load('spider.png')


def p1_score(score11):
    score11_val = s_font.render("score of player1 in round1:  " + str(score11), True, (200, 120, 0))
    screen.blit(score11_val, (200, 300))


def P1_score(score11):
    score11_val = s_font.render("score of player1 in round2:  " + str(score11), True, (200, 120, 0))
    screen.blit(score11_val, (200, 300))


def p2_score(score12):
    score12_val = s_font.render("score of player2 in round1:  " + str(score12), True, (200, 120, 0))
    screen.blit(score12_val, (200, 300))


def P2_score(score12):
    score12_val = s_font.render("score of player2 in round2:  " + str(score12), True, (200, 120, 0))
    screen.blit(score12_val, (200, 300))


def p1_on():
    score11_val = s_font.render("player1 game: Round1  ", True, (200, 120, 0))
    screen.blit(score11_val, (350, 10))


def p11_on():
    score11_val = s_font.render("player1 game: Round2  ", True, (200, 120, 0))
    screen.blit(score11_val, (350, 10))


def p22_on():
    score11_val = s_font.render("player2 game: Round2  ", True, (200, 120, 0))
    screen.blit(score11_val, (350, 10))


def p2_on():
    score11_val = s_font.render("player2 game: Round1 ", True, (200, 120, 0))
    screen.blit(score11_val, (350, 10))


def so(sox, soy):
    screen.blit(soimage, (sox, soy))


soimage = pygame.image.load('spider-web (1).png')
running = True
playerx = 400
playery = 600
score = 0
playerx_change = 0
playery_change = 0
mox1_change = 0.4
mox2_change = 0.4
mox3_change = 0.4
mox4_change = 0.4
mox5_change = 0.4
mox1 = random.randint(50, 750)
moy1 = 50
mox2 = random.randint(50, 750)
moy2 = 170
mox3 = random.randint(50, 750)
moy3 = 290
mox4 = random.randint(50, 750)
moy4 = 410
mox5 = random.randint(50, 750)
moy5 = 530
sox1 = random.randint(50, 750)
soy1 = 120
sox2 = random.randint(50, 750)
soy2 = 240
sox3 = random.randint(50, 750)
soy3 = 360
sox4 = random.randint(50, 750)
soy4 = 480
sox5 = random.randint(50, 750)
soy5 = 600
papa = 0
fval1 = 0
fval2 = 0
# game loop
running = True
while running:
    screen.fill((0, 255, 0))
    pygame.draw.rect(screen, (255, 255, 0), (0, 0, 800, 40))
    pygame.draw.rect(screen, (255, 255, 0), (0, 120, 800, 40))
    pygame.draw.rect(screen, (255, 255, 0), (0, 240, 800, 40))
    pygame.draw.rect(screen, (255, 255, 0), (0, 360, 800, 40))
    pygame.draw.rect(screen, (255, 255, 0), (0, 480, 800, 40))
    pygame.draw.rect(screen, (255, 255, 0), (0, 600, 800, 40))

    player(playerx, playery)

    if papa == 0:
        p1_on()
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                running = False

            if event.type == pygame.KEYDOWN:
                if event.key == pygame.K_LEFT and playerx >= 50:
                    playerx_change = -10
                if event.key == pygame.K_RIGHT and playerx <= 750:
                    playerx_change = 10
                if event.key == pygame.K_UP and playery >= 00:
                    playery_change = -10
                if event.key == pygame.K_DOWN and playery <= 600:
                    playery_change = 10
            if event.type == pygame.KEYUP:
                if event.key == pygame.K_LEFT or event.key == pygame.K_RIGHT:
                    playerx_change = 0
                if event.key == pygame.K_UP or event.key == pygame.K_DOWN:
                    playery_change = 0
            playerx += playerx_change
            playery += playery_change
        if math.fabs(playerx - mox5) <= 41 and math.fabs(playery - moy5) <= 42:
            playery = 600
            playerx = 400
            score = 10
            fval1 += score
            papa = 1
        if math.fabs(playerx - mox4) <= 41 and math.fabs(playery - moy4) <= 42:
            playery = 600
            playerx = 400
            score = 20
            fval1 += score
            papa = 1
        if math.fabs(playerx - mox3) <= 41 and math.fabs(playery - moy3) <= 42:
            playery = 600
            playerx = 400
            score = 40
            fval1 += score
            papa = 1
        if math.fabs(playerx - mox2) <= 41 and math.fabs(playery - moy2) <= 42:
            playery = 600
            playerx = 400
            score = 60
            fval1 += score
            papa = 1
        if math.fabs(playerx - mox1) <= 41 and math.fabs(playery - moy1) <= 42:
            playery = 600
            playerx = 400
            score = 80
            fval1 += score
            papa = 1
        if math.fabs(playerx - sox5) <= 30 and math.fabs(playery - soy5) <= 30:
            playery = 600
            playerx = 400
            score = 0
            fval1 += score
            papa = 1
            # if math.fabs(playerx - sox4) <= 30 and math.fabs(playery - soy4) <= 30:
            #   playery = 600
            #  playerx = 400
        if math.fabs(playerx - sox3) <= 30 and math.fabs(playery - soy3) <= 30:
            playery = 600
            playerx = 400
            score = 30
            fval1 += score
            papa = 1
        if math.fabs(playerx - sox2) <= 30 and math.fabs(playery - soy2) <= 30:
            playery = 600
            playerx = 400
            score = 50
            fval1 += score
            papa = 1
        if math.fabs(playerx - sox1) <= 30 and math.fabs(playery - soy1) <= 30:
            playery = 600
            playerx = 400
            score = 70
            fval1 += score
            papa = 1
        if playery == 20:
            playery = 600
            playerx = 400
            score = 90
            fval1 += score
            papa = 1

        mox1 += mox1_change
        if mox1 <= 0:
            mox1_change = 0.1
        elif mox1 >= 750:
            mox1_change = -0.1
        mox2 += mox2_change
        if mox2 <= 0:
            mox2_change = 0.1
        elif mox2 >= 750:
            mox2_change = -0.1
        mox3 += mox3_change
        if mox3 <= 0:
            mox3_change = 0.1
        elif mox3 >= 750:
            mox3_change = -0.1
        mox4 += mox4_change
        if mox4 <= 0:
            mox4_change = 0.1
        elif mox4 >= 750:
            mox4_change = -0.1
        mox5 += mox5_change
        if mox5 <= 0:
            mox5_change = 0.1
        elif mox5 >= 750:
            mox5_change = -0.1

        mo(mox1, moy1)
        mo(mox2, moy2)
        mo(mox3, moy3)
        mo(mox4, moy4)
        mo(mox5, moy5)
        so(sox1, soy1)
        so(sox2, soy2)
        so(sox3, soy3)
        # so(soy4, soy4)
        so(sox5, soy5)

    if papa == 1:
        screen.fill((0, 255, 0))
        p1_score(score)
        #fval1 += score
        #abhi=s_font.render(str(fval1),True,(255,255,255))
        # screen.blit(abhi, (400,300))
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                running = False
            if event.type == pygame.KEYDOWN:
                if event.key == pygame.K_SPACE:
                    papa = 2
    if papa == 2:
        p2_on()
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                running = False
            if event.type == pygame.KEYDOWN:
                if event.key == pygame.K_LEFT and playerx >= 50:
                    playerx_change = -10
                if event.key == pygame.K_RIGHT and playerx <= 750:
                    playerx_change = 10
                if event.key == pygame.K_UP and playery >= 00:
                    playery_change = -10
                if event.key == pygame.K_DOWN and playery <= 600:
                    playery_change = 10
            if event.type == pygame.KEYUP:
                if event.key == pygame.K_LEFT or event.key == pygame.K_RIGHT:
                    playerx_change = 0
                if event.key == pygame.K_UP or event.key == pygame.K_DOWN:
                    playery_change = 0
            playerx += playerx_change
            playery += playery_change
        if math.fabs(playerx - mox5) <= 41 and math.fabs(playery - moy5) <= 42:
            playery = 600
            playerx = 400
            score2 = 10
            fval2 += score2
            papa = 3
        if math.fabs(playerx - mox4) <= 41 and math.fabs(playery - moy4) <= 42:
            playery = 600
            playerx = 400
            score2 = 20
            fval2 += score2
            papa = 3
        if math.fabs(playerx - mox3) <= 41 and math.fabs(playery - moy3) <= 42:
            playery = 600
            playerx = 400
            score2 = 40
            fval2 += score2
            papa = 3
        if math.fabs(playerx - mox2) <= 41 and math.fabs(playery - moy2) <= 42:
            playery = 600
            playerx = 400
            score2 = 60
            fval2 += score2
            papa = 3
        if math.fabs(playerx - mox1) <= 41 and math.fabs(playery - moy1) <= 42:
            playery = 600
            playerx = 400
            score2 = 80
            fval2 += score2
            papa = 3
        if math.fabs(playerx - sox5) <= 30 and math.fabs(playery - soy5) <= 30:
            playery = 600
            playerx = 400
            score2 = 0
            fval2 += score2
            papa = 3
            # if math.fabs(playerx - sox4) <= 30 and math.fabs(playery - soy4) <= 30:
            #   playery = 600
            #  playerx = 400
        if math.fabs(playerx - sox3) <= 30 and math.fabs(playery - soy3) <= 30:
            playery = 600
            playerx = 400
            score2 = 30
            fval2 += score2
            papa = 3
        if math.fabs(playerx - sox2) <= 30 and math.fabs(playery - soy2) <= 30:
            playery = 600
            playerx = 400
            score2 = 50
            fval2 += score2
            papa = 3
        if math.fabs(playerx - sox1) <= 30 and math.fabs(playery - soy1) <= 30:
            playery = 600
            playerx = 400
            score2 = 70
            fval2 += score2
            papa = 3
        if playery == 20:
            playery = 600
            playerx = 400
            score2 = 90
            fval2 += score2
            papa = 3

        mox1 += mox1_change
        if mox1 <= 0:
            mox1_change = 0.1
        elif mox1 >= 750:
            mox1_change = -0.1
        mox2 += mox2_change
        if mox2 <= 0:
            mox2_change = 0.1
        elif mox2 >= 750:
            mox2_change = -0.1
        mox3 += mox3_change
        if mox3 <= 0:
            mox3_change = 0.1
        elif mox3 >= 750:
            mox3_change = -0.1
        mox4 += mox4_change
        if mox4 <= 0:
            mox4_change = 0.1
        elif mox4 >= 750:
            mox4_change = -0.1
        mox5 += mox5_change
        if mox5 <= 0:
            mox5_change = 0.1
        elif mox5 >= 750:
            mox5_change = -0.1

        mo(mox1, moy1)
        mo(mox2, moy2)
        mo(mox3, moy3)
        mo(mox4, moy4)
        mo(mox5, moy5)
        so(sox1, soy1)
        so(sox2, soy2)
        so(sox3, soy3)
        # so(soy4, soy4)
        so(sox5, soy5)
    if papa == 3:
        screen.fill((0, 255, 0))
        p2_score(score2)
        #fval2 += score2
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                running = False
            if event.type == pygame.KEYDOWN:
                if event.key == pygame.K_SPACE:
                    papa = 4
    if papa == 4:
        p11_on()
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                running = False

            if event.type == pygame.KEYDOWN:
                if event.key == pygame.K_LEFT and playerx >= 50:
                    playerx_change = -10
                if event.key == pygame.K_RIGHT and playerx <= 750:
                    playerx_change = 10
                if event.key == pygame.K_UP and playery >= 00:
                    playery_change = -10
                if event.key == pygame.K_DOWN and playery <= 600:
                    playery_change = 10
            if event.type == pygame.KEYUP:
                if event.key == pygame.K_LEFT or event.key == pygame.K_RIGHT:
                    playerx_change = 0
                if event.key == pygame.K_UP or event.key == pygame.K_DOWN:
                    playery_change = 0
            playerx += playerx_change
            playery += playery_change
        if math.fabs(playerx - mox5) <= 41 and math.fabs(playery - moy5) <= 42:
            playery = 600
            playerx = 400
            score = 10
            fval1 += score
            papa = 5
        if math.fabs(playerx - mox4) <= 41 and math.fabs(playery - moy4) <= 42:
            playery = 600
            playerx = 400
            score = 20
            fval1 += score
            papa = 5
        if math.fabs(playerx - mox3) <= 41 and math.fabs(playery - moy3) <= 42:
            playery = 600
            playerx = 400
            score = 40
            fval1 += score
            papa = 5
        if math.fabs(playerx - mox2) <= 41 and math.fabs(playery - moy2) <= 42:
            playery = 600
            playerx = 400
            score = 60
            fval1 += score
            papa = 5
        if math.fabs(playerx - mox1) <= 41 and math.fabs(playery - moy1) <= 42:
            playery = 600
            playerx = 400
            score = 80
            fval1 += score
            papa = 5
        if math.fabs(playerx - sox5) <= 30 and math.fabs(playery - soy5) <= 30:
            playery = 600
            playerx = 400
            score = 0
            fval1 += score
            papa = 5
            # if math.fabs(playerx - sox4) <= 30 and math.fabs(playery - soy4) <= 30:
            #   playery = 600
            #  playerx = 400
        if math.fabs(playerx - sox3) <= 30 and math.fabs(playery - soy3) <= 30:
            playery = 600
            playerx = 400
            score = 30
            fval1 += score
            papa = 5
        if math.fabs(playerx - sox2) <= 30 and math.fabs(playery - soy2) <= 30:
            playery = 600
            playerx = 400
            score = 50
            fval1 += score
            papa = 5
        if math.fabs(playerx - sox1) <= 30 and math.fabs(playery - soy1) <= 30:
            playery = 600
            playerx = 400
            score = 70
            fval1 += score
            papa = 5
        if playery == 20:
            playery = 600
            playerx = 400
            score = 90
            fval1 += score
            papa = 5

        mox1 += mox1_change
        if mox1 <= 0:
            mox1_change = 0.4
        elif mox1 >= 750:
            mox1_change = -0.4
        mox2 += mox2_change
        if mox2 <= 0:
            mox2_change = 0.4
        elif mox2 >= 750:
            mox2_change = -0.4
        mox3 += mox3_change
        if mox3 <= 0:
            mox3_change = 0.4
        elif mox3 >= 750:
            mox3_change = -0.4
        mox4 += mox4_change
        if mox4 <= 0:
            mox4_change = 0.4
        elif mox4 >= 750:
            mox4_change = -0.4
        mox5 += mox5_change
        if mox5 <= 0:
            mox5_change = 0.4
        elif mox5 >= 750:
            mox5_change = -0.4

        mo(mox1, moy1)
        mo(mox2, moy2)
        mo(mox3, moy3)
        mo(mox4, moy4)
        mo(mox5, moy5)
        so(sox1, soy1)
        so(sox2, soy2)
        so(sox3, soy3)
        # so(soy4, soy4)
        so(sox5, soy5)
    if papa == 5:
        screen.fill((0, 255, 0))
        P1_score(score)
        #fval1 += score
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                running = False
            if event.type == pygame.KEYDOWN:
                if event.key == pygame.K_SPACE:
                    papa = 6
    if papa == 6:
        p22_on()
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                running = False
            if event.type == pygame.KEYDOWN:
                if event.key == pygame.K_LEFT and playerx >= 50:
                    playerx_change = -10
                if event.key == pygame.K_RIGHT and playerx <= 750:
                    playerx_change = 10
                if event.key == pygame.K_UP and playery >= 00:
                    playery_change = -10
                if event.key == pygame.K_DOWN and playery <= 600:
                    playery_change = 10
            if event.type == pygame.KEYUP:
                if event.key == pygame.K_LEFT or event.key == pygame.K_RIGHT:
                    playerx_change = 0
                if event.key == pygame.K_UP or event.key == pygame.K_DOWN:
                    playery_change = 0
            playerx += playerx_change
            playery += playery_change
        if math.fabs(playerx - mox5) <= 41 and math.fabs(playery - moy5) <= 42:
            playery = 600
            playerx = 400
            score2 = 10
            fval2 += score2
            papa = 7
        if math.fabs(playerx - mox4) <= 41 and math.fabs(playery - moy4) <= 42:
            playery = 600
            playerx = 400
            score2 = 20
            fval2 += score2
            papa = 7
        if math.fabs(playerx - mox3) <= 41 and math.fabs(playery - moy3) <= 42:
            playery = 600
            playerx = 400
            score2 = 40
            fval2 += score2
            papa = 7
        if math.fabs(playerx - mox2) <= 41 and math.fabs(playery - moy2) <= 42:
            playery = 600
            playerx = 400
            score2 = 60
            fval2 += score2
            papa = 7
        if math.fabs(playerx - mox1) <= 41 and math.fabs(playery - moy1) <= 42:
            playery = 600
            playerx = 400
            score2 = 80
            fval2 += score2
            papa = 7
        if math.fabs(playerx - sox5) <= 30 and math.fabs(playery - soy5) <= 30:
            playery = 600
            playerx = 400
            score2 = 0
            fval2 += score2
            papa = 7
            # if math.fabs(playerx - sox4) <= 30 and math.fabs(playery - soy4) <= 30:
            #   playery = 600
            #  playerx = 400
        if math.fabs(playerx - sox3) <= 30 and math.fabs(playery - soy3) <= 30:
            playery = 600
            playerx = 400
            score2 = 30
            fval2 += score2
            papa = 7
        if math.fabs(playerx - sox2) <= 30 and math.fabs(playery - soy2) <= 30:
            playery = 600
            playerx = 400
            score2 = 50
            fval2 += score2
            papa = 7
        if math.fabs(playerx - sox1) <= 30 and math.fabs(playery - soy1) <= 30:
            playery = 600
            playerx = 400
            score2 = 70
            fval2 += score2
            papa = 7
        if playery == 20:
            playery = 600
            playerx = 400
            score2 = 90
            fval2 += score2
            papa = 7

        mox1 += mox1_change
        if mox1 <= 0:
            mox1_change = 0.4
        elif mox1 >= 750:
            mox1_change = -0.4
        mox2 += mox2_change
        if mox2 <= 0:
            mox2_change = 0.4
        elif mox2 >= 750:
            mox2_change = -0.4
        mox3 += mox3_change
        if mox3 <= 0:
            mox3_change = 0.4
        elif mox3 >= 750:
            mox3_change = -0.4
        mox4 += mox4_change
        if mox4 <= 0:
            mox4_change = 0.4
        elif mox4 >= 750:
            mox4_change = -0.4
        mox5 += mox5_change
        if mox5 <= 0:
            mox5_change = 0.4
        elif mox5 >= 750:
            mox5_change = -0.4

        mo(mox1, moy1)
        mo(mox2, moy2)
        mo(mox3, moy3)
        mo(mox4, moy4)
        mo(mox5, moy5)
        so(sox1, soy1)
        so(sox2, soy2)
        so(sox3, soy3)
        # so(soy4, soy4)
        so(sox5, soy5)
    if papa == 7:
        screen.fill((0, 255, 0))
        P2_score(score2)
        #fval2 += score2
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                running = False
            if event.type == pygame.KEYDOWN:
                if event.key == pygame.K_SPACE:
                    papa = 8
    if papa == 8:
        screen.fill((0, 255, 0))
        if fval1 > fval2:
            score_val = s_font.render("PLAYER1 WON", True, (255, 0, 0))
            screen.blit(score_val, (300, 300))
        if fval1 < fval2:
            score_val = s_font.render("PLAYER2 WON", True, (255, 0, 0))
            screen.blit(score_val, (300, 300))
        if fval1 == fval2:
            score_val = s_font.render("GAME DRAWN", True, (255, 0, 0))
            screen.blit(score_val, (300, 300))
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                running = False
            if event.type == pygame.KEYDOWN:
                if event.key == pygame.K_SPACE:
                    running = False
    pygame.display.update()
